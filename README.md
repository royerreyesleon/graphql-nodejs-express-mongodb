GraphQL, Mongodb Nodejs y Express | Conexión Simple, Consultas y Mutaciones

https://www.youtube.com/watch?v=C82btqFgcqI

mutation{
  createCar(name:"Tesla Ultimate"){
    _id
    name
  }
}

{
  allCars{
    _id
    name
  }
}

# GraphQL with Nodejs, Express and Apollo
![](screenshot.png)
![](screenshot2.png)
![](screenshot3.png)