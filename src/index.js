/* EXPRESS */
import express from 'express';
const app = express();

/* MONGODB */
import mongoose from 'mongoose';
// mongoose.connect('mongodb://localhost/graphql-mongo',{useNewUrlParser: true})
// mongoose.connect('mongodb+srv://roy:1234@cluster0.uzcwv.mongodb.net/graphql-mongo?retryWrites=true&w=majority',{useNewUrlParser: true})
mongoose.connect('mongodb+srv://roy:1234@cluster0.uzcwv.mongodb.net/ejemplo_bd?retryWrites=true&w=majority',{useNewUrlParser: true})
.then(() => console.log('connected to db'))
.catch(err => console.log(err));
import Car from './models/Car';

/* APOLLO */
import { ApolloServer } from 'apollo-server-express';
// ES COMO VAN A LUCIR LOS DATOS.
import typeDefs from './schema';
// LAS OPERACIONES QUE PODEMOS REALIZAR CON LOS DATOS.
import resolvers from './resolvers';


// settings
app.set('port', process.env.PORT || 3000);

const SERVER = new ApolloServer({
  typeDefs,
  resolvers,
  context: {
      Car
  },
  introspection: true,
  playground: true,
  playground: {
      endpoint: `http://localhost:3000/graphql`,
      settings: {
          'editor.theme': 'dark'
      }
  }
})

SERVER.applyMiddleware({
  app
})

/* EXPRESS */
// start the server
app.listen(app.get('port'), () => {
  console.log('server on port', app.get('port'));
});
