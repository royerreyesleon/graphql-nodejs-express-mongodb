import mongoose from 'mongoose';
const Schema = mongoose.Schema;

// {_is : 123abc, name: "Tesla"}
const CarSChema = new Schema({
  name: String
});

const Car = mongoose.model('cars', CarSChema);

export default Car;
